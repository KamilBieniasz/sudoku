package com.example.sudoku.dto;

import java.util.ArrayList;
import java.util.List;

public class SudokuDto {
    public List<Integer> lineIds = new ArrayList<>();
    public List<Integer> columnIds = new ArrayList<>();
    public List<Integer> areaIds = new ArrayList<>();

    public SudokuDto() {
    }

    public SudokuDto(List<Integer> lineIds, List<Integer> columnIds, List<Integer> areaIds) {
        this.lineIds = lineIds;
        this.columnIds = columnIds;
        this.areaIds = areaIds;
    }

    public List<Integer> getLineIds() {
        return lineIds;
    }

    public void setLineIds(List<Integer> lineIds) {
        this.lineIds = lineIds;
    }

    public List<Integer> getColumnIds() {
        return columnIds;
    }

    public void setColumnIds(List<Integer> columnIds) {
        this.columnIds = columnIds;
    }

    public List<Integer> getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(List<Integer> areaIds) {
        this.areaIds = areaIds;
    }
}
