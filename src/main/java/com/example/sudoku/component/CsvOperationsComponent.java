package com.example.sudoku.component;

import java.util.List;

public interface CsvOperationsComponent {

    String PATH = "E:/Programowanie/Java/sudoku/";

    List<String> readCsvFile(String fileName, String divider);
}
